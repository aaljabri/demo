# Project Title

Frontend Starter 

## Description

A Next.js starter that includes all you need to build amazing projects

## Getting Started

Clone the project and enjoy 🔥

## Features

- ⚡️ Next.js 13
- ⚛️ React 18
- ⛑ TypeScript 4
- 📏 ESLint — To find and fix problems in your code
- 💖 Prettier — Code Formatter for consistent style
- 💨 Tailwind 3 — A utility-first CSS framework 
- 🃏 Jest — Configured for unit testing
- 🐶 Husky — For running scripts before committing
- 🚓 Commitlint — To make sure your commit messages follow the conventional commit
- 🚫 lint-staged — Run ESLint and Prettier against staged Git files
- 🗂 Absolute import  — Import components or images using the `@` prefix
- 🎩 NextAuth (Auth0Provider)  — Authentication
- 🗺 React-i18next  — Powerful internationalization framework
- 📦 Dockerfile  — Generate containers automatically
- 💯 Gitlab CI —  To automates all the steps required to test, build and deploy your code
- ⚡️  Axios —  HTTP client



## Development


### Requirements

- Node.js 
- Yarn
- Docker
- Docker-compose

## Documentation


### Directory Structure

- [`.husky`](.husky) — Husky configuration and hooks<br>
- [`public`](./public) — Static assets such as fonts, images, styles, locales and favicon<br>
- [`src`](./src) — Application source code, including pages, components, hooks, routes, types and utils<br>
- [`tests`](./tests) — Tests including units tests and E2E test<br>

### Scripts

- `yarn dev` — Starts the application in development mode at `http://localhost:3000`
- `yarn build` — Creates an optimized production build of your application
- `yarn start` — Starts the application in production mode
- `yarn lint` — Runs ESLint for all files in the `src` directory
- `yarn format` — Runs Prettier for all files in the `src` directory
- `yarn type-check` — Validate code using TypeScript compiler
- `yarn test` — Runs all tests.


## Things to be added later:
- `SonarQube Code Analysis` —  To perform automatic reviews with static analysis of code to detect bugs, code smells and security vulnerabilities
- `semantic-release`  —   To determine the consumer impact of changes in the codebase
- `docker-compose`  —   To run local environment