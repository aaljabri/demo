import { useTranslation } from 'react-i18next'

export default function Home() {
  const { t } = useTranslation()

  return (
    <div className="flex items-center justify-center h-screen">
      <main className="flex flex-col p-2 ">
        <div className="flex items-center space-x-2 ">
          <h2 className="font-bold text-blue-500">Hey, {t('welcome')} </h2>
        </div>
      </main>
    </div>
  )
}
