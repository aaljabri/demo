import axios, { AxiosInstance } from 'axios'

// eslint-disable-next-line @typescript-eslint/no-unused-vars
abstract class HttpClient {
  protected readonly instance: AxiosInstance

  public constructor(baseURL: string) {
    this.instance = axios.create({
      baseURL,
    })
  }
}
