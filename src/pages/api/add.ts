// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import prisma from 'src/libs/prisma'

export default async function handler(
  req: NextApiRequest,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  res: NextApiResponse<any>
) {
  await prisma.user.create({
    data: {
      email: 'moh@deem.sa',
      firstName: 'moh',
      lastName: 'Aljabri',
    },
  })
  await prisma.user.create({
    data: {
      email: 'rakan@deem.sa',
      firstName: 'Rakan',
      lastName: 'Aljabri',
    },
  })
  res.status(200).json('user created')
}
