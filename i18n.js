import i18n from 'i18next'
import { initReactI18next } from 'react-i18next'

import commonAr from '@/locales/ar/common.json'
import commonEn from '@/locales/en/common.json'

const resources = {
  en: {
    translation: {
      ...commonEn,
    },
  },
  ar: {
    translation: {
      ...commonAr,
    },
  },
}

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources,
    lng: 'en',
    interpolation: {
      escapeValue: false, // react already safes from xss
    },
  })

export default i18n
